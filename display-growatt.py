#!/usr/bin/env python

import time
import scrollphathd
from scrollphathd.fonts import font3x5
import sqlite3
from typing import Any, Union

# default brightness
BRIGHTNESS = 0.2

# database connection
conn = sqlite3.connect('growatt/growatt.db')

scrollphathd.rotate(180)

# Specify the JSON file path
json_file = "growatt/growatt.json"

print("""
Display growatt data in a 3x5 pixel condensed font.

Press Ctrl+C to exit!

""")


def display_and_scroll(text):
    scrollphathd.clear()
    text = "     " + text + "     "
    display_len = scrollphathd.write_string(text, y=1, font=font3x5, brightness=BRIGHTNESS)
    counter = 0
    while counter < (display_len - scrollphathd.DISPLAY_WIDTH):
        # Show the buffer
        scrollphathd.show()
        # Scroll the buffer content
        scrollphathd.scroll()
        # Wait for 0.1s
        time.sleep(0.01)
        counter = counter + 1


def display_and_wait(text):
    scrollphathd.clear()
    display_len = scrollphathd.write_string(text, y=1, font=font3x5, brightness=BRIGHTNESS)
    scroll_offset = round((scrollphathd.DISPLAY_WIDTH - display_len) / 2)
    scrollphathd.scroll(-scroll_offset - 1, 0)
    scrollphathd.show()
    time.sleep(5)


def plot_graph(column: str, min_value: Union[int, float] = 0, max_value: Union[int, float] = 0):
    scrollphathd.clear()
    cursor = conn.cursor()
    query = f"SELECT {column} FROM STATUS ORDER BY _TIMESTAMP DESC LIMIT {scrollphathd.DISPLAY_WIDTH}"
    # Execute the SQL query
    cursor.execute(query)
    # Fetch all the results into a list of integers or floats
    data = []
    list = cursor.fetchall()
    for row in list:
        value = row[0]
        data.append(float(value))
    # Close the cursor and the database connection when you're done
    cursor.close()
    data.reverse()
    # Bprint(data)
    if min_value == 0 and max_value == 0:
        min_value = min(data)
        max_value = max(data)
        if min_value == max_value:
            max_value = max_value + 0.1
    scrollphathd.set_graph(data, low=min_value, high=max_value, brightness=BRIGHTNESS)
    scrollphathd.show()
    time.sleep(5)


while True:

    # Create the SELECT query
    select_query = "SELECT * FROM STATUS ORDER BY _TIMESTAMP DESC LIMIT 1"
    # Execute the SELECT query
    result = conn.execute(select_query)
    # _TIMESTAMP, SOLAR, SOC, DISCHARGE, CHARGE, IMP, EXP, LOAD
    data = result.fetchone()

    # Now 'data' contains the dictionary with the loaded data
    print(data)
    print("\n")

    solar = float(data[1])
    soc = int(data[2])
    pdisCharge1 = float(data[3])
    chargePower = float(data[4])
    imp = float(data[5])
    exp = float(data[6])
    load = data[7]
    # conn.execute(insert_command, (current_timestamp, solar, soc, pdisCharge1, chargePower, imp, exp, load))
    # Solar output
    if solar > 0:
        display_and_scroll("SOLAR:")
        display_and_wait(f"{solar}")
        plot_graph("SOLAR")

    # Battery status if charing or discharging
    if pdisCharge1 > 0 or chargePower > 0 or soc > 25:
        display_and_scroll("BATTERY:")
        display_and_wait(f"{soc}%")
        plot_graph("SOC")

    # Discharging battery
    if pdisCharge1 > 0:
        display_and_scroll("DISCHARGING:")
        display_and_wait(f"{pdisCharge1}")
        #plot_graph("DISCHARGE")

    # Charging battery
    if chargePower > 0:
        display_and_scroll("CHARGING:")
        display_and_wait(f"{chargePower}")
        #plot_graph("CHARGE")

    # Power load of home
    display_and_scroll("LOAD:")
    display_and_wait(f"{load}")
    #plot_graph("LOAD")

    # Import from grid
    if imp > 0:
        display_and_scroll("IMPORT:")
        display_and_wait(f"{imp}")
        #plot_graph("IMP")

    # Export to grid
    if exp > 0:
        display_and_scroll("EXPORT:")
        display_and_wait(f"{exp}")
