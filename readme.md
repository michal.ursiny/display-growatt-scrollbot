Growatt inverter display status for Pimoroni ScrollBot
======================================================

Possibly other 17x7 Scroll Hats from Pimoroni controlled via `scrollphathd` python library https://github.com/pimoroni/scroll-phat-hd

Also uses growattServer python library: https://pypi.org/project/growattServer/

Tested on GROWATT SPH 10000TL3 BH-UP, displays:
- solar power generated in kW -> if any also chart
- battery soc status -> if >25 and also either charging or discharging -> and chart as well
- if charging, then charging rate in kW
- if discharging, then discharging rate in kW
- load of house in kW
- if import, then import from grid in kW
- if export, then also export to grid in kW

kW is not displayed as per limited display

data are updated each 5mins, as more often doesn't make sense; this is not REALTIME, this is just like mobile app

![Example running Growatt display](display-growatt.gif)

Setup!

1. First install `scrollphathd` python library from https://github.com/pimoroni/scroll-phat-hd and make sure it works mainly pythons scripts from examples
2. `git clone` this repo into `Pimoroni/scrollphathd/examples`
3. edit `display-growatt.sh` and `growatt/fetch_growatt.sh` as well as `.bash_login` so it fits your absolute paths
4. rename `growatt/config_backup.py` to `growatt/config.py` and edit with your server.growatt.com credentials
5. `cd growatt` and `pip install growattServer`
6. while there run `python3 create_db.py` to setup sqlite3 db

- at this point you should be able to run `fetch_growatt.py` which will fetch your inverter data and put them into SqlLite3 database for later purposes -> you should see JSON output; note: you may chmod that .sh files to make them executable

now setup this into system, so it runs continually
7. run `crontab -e` and add:
```
*/5 * * * * sh /home/_your_username_/Pimoroni/scrollphathd/examples/growatt/fetch_growatt.sh
```
to make it run every 5 mins - every 5 mins downloads your inverter status, parse it and store in sqlite3 db (you created step 6)
8. copy `.bash_login` script you edited in #3 to your home directory -> this will launch `display-growatt.sh` after your (auto)login via screen virtual console; thus make sure you have screen installed, if not then `brew install screen`

hopefully this whole thing works for you!

