#!/usr/bin/env python

import sqlite3

conn = sqlite3.connect('growatt.db')
print("Opened database successfully")

# solar = float(data["ppv"])
# pdisCharge1 = float(data["pdisCharge1"])
# chargePower = float(data["chargePower"])
# imp = float(data["pactouser"])
# exp = float(data["pactogrid"])
# soc = int(data["SOC"])
# load = data["pLocalLoad"]

conn.execute('''CREATE TABLE STATUS
       (_TIMESTAMP INTEGER PRIMARY KEY NOT NULL,
        SOLAR FLOAT NOT NULL DEFAULT 0,
        SOC INTEGER NOT NULL DEFAULT 0,
        DISCHARGE FLOAT NOT NULL DEFAULT 0,
        CHARGE FLOAT NOT NULL DEFAULT 0,
        IMP FLOAT NOT NULL DEFAULT 0,
        EXP FLOAT NOT NULL DEFAULT 0,
        LOAD FLOAT NOT NULL DEFAULT 0)''')

print("Table created successfully")

conn.close()
