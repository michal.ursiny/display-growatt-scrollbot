#!/usr/bin/env python

import growattServer
import sqlite3
import time
from config import GROWATT_USERNAME, GROWATT_PASSWORD  # Import credentials

api = growattServer.GrowattApi()
login_response = api.login(GROWATT_USERNAME, GROWATT_PASSWORD)
# Get a list of growatt plants.
plants = api.plant_list(login_response['user']['id'])
#print(plants['data'][0]['plantId'])
plant_id = plants['data'][0]['plantId']
#print(plants)
#plant = api.plant_info(plant_id)
#print(plant)
#devices = api.device_list(plant_id)
#print(devices)
#inverter = api.inverter_detail("TPJ5CMD13Z")
#print(inverter)
data = api.mix_system_status("TPJ5CMD13Z", plant_id)
print(data)

solar = float(data["ppv"])
pdisCharge1 = float(data["pdisCharge1"])
chargePower = float(data["chargePower"])
imp = float(data["pactouser"])
exp = float(data["pactogrid"])
soc = int(data["SOC"])
load = data["pLocalLoad"]
current_timestamp = int(time.time())

conn = sqlite3.connect('growatt.db')
# Create the INSERT command
insert_command = f"INSERT INTO STATUS (_TIMESTAMP, SOLAR, SOC, DISCHARGE, CHARGE, IMP, EXP, LOAD) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)"
# Execute the INSERT command with the data
conn.execute(insert_command, (current_timestamp, solar, soc, pdisCharge1, chargePower, imp, exp, load))
# Commit the changes to the database
conn.commit()
# Close the database connection when you're done
conn.close()